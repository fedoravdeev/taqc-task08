package main.org.fedoravdeev.fibonacciseries;

import static java.lang.Math.pow;

public class Fibonacci {

    private final static String SEPARATOR = ",";
    private final static String EMPTY_FIBONACCI_ROW = "this is empty fibonacci row";
    private final static String MAX_INT = "Tne end of range more then 2 147 483 647";
    private final static int TWO = 2;
    private final static int TEN = 10;
    private final static int UINT_MAXUINT_MAX = 2147483647;

    public String printFibo(int lengthSymbols) {

        if (!(lengthSymbols > 0)) {
            return EMPTY_FIBONACCI_ROW;
        }

        StringBuilder stringFibo = new StringBuilder();
        int startRange = getStartRange(lengthSymbols);
        int endRange = getEndRange(lengthSymbols);
        stringFibo.append(printFibo(startRange, endRange));

        return stringFibo.toString();
    }

    public String printFibo(int startRange, int endRange) {

        if (!(startRange <= endRange)) {
            return EMPTY_FIBONACCI_ROW;
        }
        if (!(startRange >= 0 && endRange >= 0)) {
            return EMPTY_FIBONACCI_ROW;
        }
        if(endRange >= UINT_MAXUINT_MAX){
            return MAX_INT;
        }

        StringBuilder stringFibo = new StringBuilder();
        int prev = 0;
        int curr = 0;
        for (int i = 0; curr <= endRange; i++) {
            if (i < TWO) {
                curr = i;
            } else {
                curr += prev;
                prev = curr - prev;
            }
            if (curr >= startRange && curr <= endRange) {
                stringFibo.append(curr).append(SEPARATOR);
            }
        }
        if (stringFibo.length() > 0) {
            stringFibo.delete(stringFibo.length() - 1, stringFibo.length());
        }
        return stringFibo.toString();
    }

    private int getEndRange(int elementArr) {
        return (int) pow(TEN, elementArr) - 1;
    }

    private int getStartRange(int elementArr) {
        return elementArr == 1 ? 0 : (int) pow(TEN, elementArr - 1);
    }

    private void printHelp() {
        System.out.println("The program print Fibonacci row that are specified by the parameters:\n" +
                " (int)      - print Fibonacci row with the specified length,\n" +
                " (int, int) - print Fibonacci row in the specified range.");
    }

    public void run(String[] args) {

        if (args.length >= 1 && args.length <= 2) {

            switch (args.length) {
                case 1:
                    int lengthSymbols = (int) Integer.parseInt(args[0]);
                    System.out.println(printFibo(lengthSymbols));
                    break;
                case 2:
                    int startRange = (int) Integer.parseInt(args[0]);
                    int endRange = (int) Integer.parseInt(args[1]);
                    System.out.println(printFibo(startRange, endRange));
                    break;

            }
        } else {
            printHelp();
        }
    }
}
