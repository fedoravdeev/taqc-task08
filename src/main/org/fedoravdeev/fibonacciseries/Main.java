package main.org.fedoravdeev.fibonacciseries;

public class Main {

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci();
        fibonacci.run(args);
    }
}
