package test.org.fedoravdeev.fibonacciseries;

import main.org.fedoravdeev.fibonacciseries.Fibonacci;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FibonacciTest extends Fibonacci {

    @Test
    @DisplayName("should be print Fibonacci row with the specified length 1")
    public void printFibonacciSpecifyLength1() {
        String expected = "0,1,1,2,3,5,8";
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.printFibo(1);
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print Fibonacci row with the specified length 2")
    public void printFibonacciSpecifyLength2() {
        String expected = "13,21,34,55,89";
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.printFibo(2);
        Assert.assertEquals(actual, expected);
    }
    @Test
    @DisplayName("should be print Fibonacci row with the specified length 3")
    public void printFibonacciSpecifyLength() {
        String expected = "144,233,377,610,987";
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.printFibo(3);
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print Fibonacci row in the specified range from 0 to 9")
    public void printFibonacciRange0() {
        String expected = "0,1,1,2,3,5,8";
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.printFibo(0,9);
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print Fibonacci row in the specified range from 10 to 99")
    public void printFibonacciRange10() {
        String expected = "13,21,34,55,89";
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.printFibo(10,99);
        Assert.assertEquals(actual, expected);
    }

    @Test
    @DisplayName("should be print Fibonacci row in the specified range from 100 to 999")
    public void printFibonacciRange100() {
        String expected = "144,233,377,610,987";
        Fibonacci fibonacci = new Fibonacci();
        String actual = fibonacci.printFibo(100,999);
        Assert.assertEquals(actual, expected);
    }
}
